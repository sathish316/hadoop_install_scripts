# Add CDH sources
$ sudo nano /etc/apt/sources.list.d/cloudera.list
deb [arch=amd64] http://archive.cloudera.com/cdh4/ubuntu/precise/amd64/cdh precise-cdh4 contrib
deb-src http://archive.cloudera.com/cdh4/ubuntu/precise/amd64/cdh precise-cdh4 contrib
$ sudo apt-get -y install curl
$ curl -s http://archive.cloudera.com/cdh4/ubuntu/precise/amd64/cdh/archive.key | sudo apt-key add - 

# Add Java 7 sources
$ sudo apt-get update
$ sudo apt-get -y install python-software-properties
$ sudo add-apt-repository ppa:webupd8team/java
$ sudo apt-get update
$ sudo apt-get -y install oracle-java7-installer
$ export JAVA_HOME=/usr/lib/jvm/java-7-oracle
#TODO: automate oracle license accept

# Install Hadoop, HBase, Zookeeper
$ sudo apt-get install -y hadoop-conf-pseudo
$ sudo apt-get install hadoop-0.20-mapreduce-jobtracker
$ sudo apt-get install hadoop-0.20-mapreduce-tasktracker

$ ./format_namenode.sh
$ ./start_hdfs.sh
$ ./setup_hdfs_dirs.sh
$ ./setup_yarn_dirs.sh
$ ./setup_hbase_dirs.sh

$ sudo apt-get install hbase
$ sudo apt-get install hbase-master
