#!/bin/bash
for service in /etc/init.d/hadoop-mapreduce-* /etc/init.d/hadoop-yarn-*
do
  sudo $service stop
done
